import memcache, random, time, timeit
from math import *
mc = memcache.Client(['194.29.175.241:11211','194.29.175.242:11211'])

found = 0
total = 0

def compute_square(n):
    global total, found
    total += 1
    value = mc.get(str(n)+"mt")
    if value is None:
        time.sleep(0.003)
        value = rozk(n)
        mc.set(str(n)+"mt", value)
    else:
        found += 1
    print str(n) + ' = ' + str(value)
    return value

def make_request():
    compute_square(random.randint(2, 1000))

def rozk(n):
    i=2
    e=floor(sqrt(n))
    t=[]
    while i<=e:
        if n%i==0:
            t.append(i)
            n/=i
            e=floor(sqrt(n))
        else:
            i+=1
    if n>1: t.append(n)
    return t

print 'Ten successive runs:',
for i in range(1, 12):
    print '%.2fs, ratio=%.2f' % (timeit.timeit(make_request, number=20), float(found)/total)
    print