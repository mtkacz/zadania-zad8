from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Tags(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Box(models.Model):
    name = models.CharField(max_length=100)
    parent = models.ForeignKey('Box', null=True)

    def __unicode__(self):
        return self.name

class Picture(models.Model):
    author = models.ForeignKey(User, related_name='+')
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='media/photo/')
    pub_date = models.DateTimeField()
    edit_date = models.DateTimeField(null=True)
    edit_user = models.ForeignKey(User, null=True)
    box = models.ForeignKey(Box)

    def __unicode__(self):
        return self.title + "\n" + self.image

    def be_edit(self):
        return ((not self.edit_user is None) or (not self.edit_date is None))

class Entry(models.Model):
    author = models.ForeignKey(User, related_name='+')
    title = models.CharField(max_length=200)
    text = models.TextField(max_length=400)
    pub_date = models.DateTimeField()
    edit_date = models.DateTimeField(null=True)
    edit_user = models.ForeignKey(User, null=True)
    tags = models.ManyToManyField(Tags)

    def __unicode__(self):
        return self.text

    def chek_user(self):
        return 1==1

    def can_edit(self):
        now = timezone.now()
        time_delta = now - self.pub_date
        return ((time_delta.minutes <= 10) and (time_delta.hour == 0) and (time_delta.days == 0) and (time_delta.month == 0) and (time_delta.year == 0))

    def be_edit(self):
        return ((not self.edit_user is None) or (not self.edit_date is None))