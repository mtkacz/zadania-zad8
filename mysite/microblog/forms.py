from django.forms import ModelForm, Textarea, CharField
from django import forms
from models import *

class EntryForm(ModelForm):
    class Meta:
        model = Picture

class PartialEntryForm(ModelForm):
    box = forms.ModelChoiceField(queryset=Box.objects.all(), widget=forms.Select())
    class Meta:
        model = Picture
        exclude = ('author', 'pub_date', 'edit_date', 'edit_user')

    def clean_text(self):
        return True

class VeryPartialEntryForm(ModelForm):
    box = forms.ModelChoiceField(queryset=Box.objects.all(), widget=forms.Select())
    class Meta:
        model = Picture
        exclude =('author', 'pub_date', 'edit_date', 'edit_user', 'title', 'image')

class BoxForm(ModelForm):
    parent = forms.ModelChoiceField(queryset=Box.objects.all(), widget=forms.Select())
    class Meta:
        model = Box